package com.example.demo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("")
public class PublicController {
    @GetMapping("")
    public String index() {

        return "<h1>Deploy heroku</h1>";
    }
}

