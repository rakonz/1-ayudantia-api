package com.example.demo.services;

import com.example.demo.dao.UserRepository;
import com.example.demo.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.RuntimeErrorException;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User createUser(User user) {
        return userRepository.save(user);
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUserById(int id) {
        return userRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    public User updateUser(int id, User user) {
        User userDB = userRepository.findById(id).orElseThrow(RuntimeException::new);
        if (user.getAddress() == null && user.getName() == null & user.getLastName() == null) {
            throw new RuntimeException("No hay usuario");
        }
        else if (user.getLastName() != null) {
            userDB.setLastName(user.getLastName());
            userDB.setName(userDB.getName());
            userDB.setAddress(userDB.getAddress());
        } else if (user.getName() != null) {
            userDB.setLastName(userDB.getLastName());
            userDB.setName(user.getName());
            userDB.setAddress(userDB.getAddress());
        } else if (user.getAddress() != null) {
            userDB.setLastName(userDB.getLastName());
            userDB.setName(userDB.getName());
            userDB.setAddress(user.getAddress());
        }
        else {
            userDB.setName(user.getName());
            userDB.setAddress(user.getAddress());
            userDB.setLastName(user.getLastName());
        }

        return userRepository.save(userDB);
    }

    public void deleteUser(int id) {
         userRepository.deleteById(id);
    }
}
